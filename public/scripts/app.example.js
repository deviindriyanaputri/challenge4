class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    this.clear();
    const passenger = document.getElementById("passenger");
    const date = document.getElementById("date");
    const time = document.getElementById("time");
    
    const passengerSeat = passenger.value;
    const totalDate = date.value;
    const totalTime = time.value;
    const inputDateTime = Date.parse(totalDate + "T" + totalTime + "Z");
    console.log(inputDateTime)

    const cars = await Binar.listCars();

    const availableCar = cars.filter((car) => {
      return car.capacity >= passengerSeat && car.available === true && Date.parse(car.availableAt) > inputDateTime
    });
    Car.init(availableCar);
    console.log(availableCar)
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
