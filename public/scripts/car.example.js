class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
        <div class="carContainer">
            <div class="card p-2" style="height: 700px;">
                <img src="${this.image}" class="card-img-top" alt="mobil-01">
                <div class="card-body">
                    <h5 class="card-title">${this.available}/${this.manufacture}/${this.model}</h5>
                    <h4><strong> Rp. ${this.rentPerDay} / hari</strong></h4>
                    <p class="card-text">${this.description} </p>
                    <p><i class="fas fa-user-friends"></i> ${this.capacity} orang</p>
                    <p><i class="fa fa-gear"></i> ${this.transmission}</p>
                    <p><i class="fa fa-calendar"></i> Tahun ${this.year}</p>
                    <button type="button" class="btn-mobil btn btn-primary">Pilih Mobil</button>
                </div>
            </div>
        </div>
    `;
  }
}
